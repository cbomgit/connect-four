from math import sqrt
from collections import namedtuple

class Board(namedtuple("Board", ["state"])):

    """
    Board class representing the game board in a certain state

    Class members:

        * state - 2 x 2 array in column major order that represents the board state
        * width - number of columns in the grid
        * height - height of each column (number of slots)
    """

    WIDTH = 7
    HEIGHT = 6

    __slots__ = ()

    def __new__(cls, state):

        return super().__new__(cls, state)

    def __hash__(self):
        return hash(str(self.state))

    def get_valid_moves(self):
        """
        Return a list of moves that can be applied to the current
        board
        """
        moves = []

        #get the next available moves by iterating over each row...
        for i in range(Board.WIDTH):

            ndx = -1
            
            #in each row, iterate over every slot until the first non-empty slot is 
            #found. if ndx > -1 that means there is at least 1 empty slot before the first
            #non empty slot and the move is legal.

            while ndx + 1 < len(self.state[i]) and self.state[i][ndx + 1] is 0:
                ndx += 1

            if ndx > -1:
                moves.append((i, ndx))

        return moves


    def get_successor(self, move, player):

        """
        Applies move to the board
        and returns a new board with the new configuraiton
        move is a tuple (col, slot), indicating the col and slot
        where the move should be placed
        """

        #create a copy of the current state
        state = [row[:] for row in self.state]
        col = move[0]
        slot = move[1]

        state[col][slot] = player

        return Board(state)
