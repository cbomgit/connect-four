from collections import deque, namedtuple

class Node(namedtuple("AStarNode", ["board", "player", "depth", "parent", "action"])):

    """
    Represents a node in a graph search algorithm. This object has a board object
    to represent its board state, a parent attribute to represent the parent, and
    a rules list to keep track of the rules left to process
    """

    __slots__ = ()

    def __new__(cls, board, player, depth, parent=None, action=None):

        return super().__new__(cls, board, player, depth, parent, action)

    def __hash__(self):
        return self.board.__hash__()

    def __eq__(self, other):
        return isinstance(other, Node) and self.board == other.board

    def __ne__(self, other):
        return self.board != other.board

    