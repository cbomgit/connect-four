import json
import argparse
from connectfour.board import Board
from connectfour.node import Node


def get_game_data():

    """
    read in and parse the game data from stdin
    """

    parser = argparse.ArgumentParser(prog="connectfour.py", description="Connect Four agent")
    game_data = json.JSONDecoder().decode(input())

    if "grid" not in game_data.keys():
        print("No game data provided - exiting")
        exit()

    if "height" not in game_data.keys():
        print("No height provided - exiting")
        exit()

    if "width" not in game_data.keys():
        print("No width provided - exiting")
        exit()

    if "player" not in game_data.keys():
        print("No player provided - exiting")
        exit()

    return game_data


def get_best_move(board, depth, current_player):

    """
    minimax algorithm implementation for finding the best
    available move given the current state and player. depth
    limits the search to n levels of look-ahead. A higher depth
    will cause the AI to take longer
    """

    opp_player = 1 if current_player is 2 else 2
    
    #get array of tuples representing legal moves in this state
    #first member of tuple is row, second member is the slot in that row
    moves = board.get_valid_moves()

    #dictionary of moves and the corresponding score of the state resulting
    #from the move
    scores = {}

    for move in moves:
        child = board.get_successor(move, current_player)
        score = -expand_move_tree(child, depth - 1, opp_player)
        scores[move[0]] = score

    best_score = -99999999
    best_move = None

    for move,score in scores.items():
        if score >= best_score:
            best_score = score
            best_move = move

    return {"move": best_move}

def expand_move_tree(board, depth, current_player):

    """
    expands the move tree and returns the minimax value for the current node
    """

    moves = board.get_valid_moves()

    #end condition for recursion
    if depth is 0 or len(moves) is 0 or is_goal_state(board.state, current_player):
        return get_value(board.state, current_player)

    #determine the opposing player
    opp_player = 1 if current_player is 2 else 2

    best_score = -99999999

    #get the minimax value for this state's children
    for move in moves:
        successor = board.get_successor(move, current_player)
        best_score = max(best_score, -expand_move_tree(successor, depth - 1, opp_player))

    return best_score


def get_value(state, current_player):

    """
    Hueristic function used to determine the best possible move
    given a state. 

    The goal of the hueristic is to heavily weight 4 in a row, 
    while weights for 3 and 2 in a row are smaller in magnitude.
    """
    opp_player = 1 if current_player is 2 else 2
    player_one_fours = count_consecutive_tiles(state, current_player, 4)
    player_one_threes = count_consecutive_tiles(state, current_player, 3)
    player_one_twos = count_consecutive_tiles(state, current_player, 2)

    player_two_fours = count_consecutive_tiles(state, opp_player, 4)

    if player_two_fours > 0:
        return -100000
    else:
        return player_one_fours * 100000 + player_one_threes * 100 + player_one_twos


def is_goal_state(state, current_player):

    """
    Returns true if this board is in a winning state for either player,
    otherwise returns false
    """

    opp_player = 1 if current_player is 2 else 2

    if count_consecutive_tiles(state, current_player, 4) > 0:
        return True

    if count_consecutive_tiles(state, opp_player, 4) > 0:
        return True

    return False


def count_consecutive_tiles(state, current_player, num_consecutive):
    """
    check for consecutive tiles in a given state for the current_player.
    Returns a count of consecutive tiles with num_consecutive tiles in a row.
    Consecutive tiles in any direction are counted
    """

    #check for vertical tiles
    total = 0

    total += count_consecutive_horizontal(state, current_player, num_consecutive)
    total += count_consecutive_vertical(state, current_player, num_consecutive)
    total += count_diagonal_tiles(state, current_player, num_consecutive)

    return total

def count_diagonal_tiles(state, current_player, num_consecutive):
    """
    counts consecutive tiles in diagonal directions
    """
    total = 0

    #count diagonals with positive slope
    for i in range(Board.WIDTH - 1):

        col = i
        slot = 0
        count = 0

        while col > -1:
            if state[col][slot] is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0
            col -= 1
            slot += 1

        if count >= num_consecutive:
            total += 1

    for i in range(Board.WIDTH - 1, 0, -1):

        col = i
        slot = Board.HEIGHT - 1
        count = 0

        while col < Board.WIDTH:
            if state[col][slot] is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0

            col += 1
            slot -= 1
        
        if count >=num_consecutive:
            total += 1

    #count diagonals with negative slope
    for i in range(Board.WIDTH - 1):
        
        col = i
        slot = Board.HEIGHT - 1
        count = 0

        while col > -1:
            if state[col][slot] is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0
            col -= 1
            slot -= 1

        if count >= num_consecutive:
            total += 1

    for i in range(Board.WIDTH - 1, 0, -1):

        col = i
        slot = 0
        count = 0

        while col < Board.WIDTH:
            if state[col][slot] is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0
            col += 1
            slot += 1

    return total

def count_consecutive_horizontal(state, current_player, num_consecutive):

    """
    Checks for consecutive tiles in the horizontal direction
    """
    total = 0

    for i in range(Board.HEIGHT):

        count = 0

        for j in range(Board.WIDTH):

            if state[j][i] is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0
        if count >= num_consecutive:
            total += 1

    return total

def count_consecutive_vertical(state, current_player, num_consecutive):

    """
    Count consecutive tiles in the vertical direction
    """
    #check for vertical tiles
    total = 0

    for row in state:

        count = 0

        for slot in row:
            if slot is current_player:
                count += 1
            else:
                if count >= num_consecutive:
                    total += 1
                count = 0

        if count >= num_consecutive:
            total += 1

    return total

game_data = get_game_data()

move = get_best_move(Board(game_data['grid']), 4, game_data['player'])
print(move)