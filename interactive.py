import argparse
import json
from board import Board
from agents import NaiveAI, MinimaxAI

def get_game_data():

    """
    read in and parse the game data from stdin
    """

    parser = argparse.ArgumentParser(prog="connectfour.py", description="Connect Four agent")
    game_data = json.JSONDecoder().decode(input())

    if "grid" not in game_data.keys():
        print("No game data provided - exiting")
        exit()

    if "height" not in game_data.keys():
        print("No height provided - exiting")
        exit()

    if "width" not in game_data.keys():
        print("No width provided - exiting")
        exit()

    if "player" not in game_data.keys():
        print("No player provided - exiting")
        exit()

    return game_data

game_data = get_game_data()
board = Board(game_data['grid'])
ai = MinimaxAI(game_data['player'])
move = ai.get_best_move(board, 4, ai.player)
print(move)