# README #

Connect four AI written in python.

### What is this repository for? ###

This repo contains a script called connectfour.py which simulates an AI game using a naive player 
and an intelligent player that selects moves using the minimax algorithm.

### How do I get set up? ###

* Clone this repository
* run python connectfour.py 

