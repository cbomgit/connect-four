import json
import argparse
import random
from board import Board
from agents import NaiveAI, MinimaxAI

game_board = Board([[0 for i in range(6)] for j in range(7)])


player_one = MinimaxAI(1)
player_two = MinimaxAI(2)
current_player = player_one
search_depth = 4
turn_num = 0


while True:

    #player one's turn
    if player_one.is_goal_state(game_board.state, player_one.player):
        print("Player 2 wins!")
        break

    if len(game_board.get_valid_moves()) is 0:
        print("No more moves - draw!")
        break

    next_move = player_one.get_best_move(game_board, search_depth, player_one.player)
    legal_moves = game_board.get_valid_moves()
    selected_move = None
    for move in legal_moves:
        if next_move['move'] == move[0]:
            selected_move  = move
    game_board = game_board.get_successor(selected_move, player_one.player)

    turn_num += 1
    game_board.pprint_board()
    print("--- Move %d ---" % turn_num)

    #player two's turn
    if player_two.is_goal_state(game_board.state, player_two.player):
        print("Player 1 wins!")
        break
    
    if len(game_board.get_valid_moves()) is 0:
        print("No more moves -draw!")
        break

    next_move = player_two.get_best_move(game_board, search_depth, player_two.player)
    legal_moves = game_board.get_valid_moves()
    selected_move = None
    for move in legal_moves:
        if next_move['move'] == move[0]:
            selected_move  = move
    game_board = game_board.get_successor(selected_move, player_two.player)

    turn_num += 1
    game_board.pprint_board()
    print("--- Move %d ---" % turn_num)
